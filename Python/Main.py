from Arduino import *
from UI import *


ard1 = Arduino('ard1', "ca:4a:27:6d:e5:29")
ard2 = Arduino('ard2', "e9:51:14:af:bb:16")

ard1.connect()
ard1.setLED(2)
ard1.disconnect()

ard2.connect()
ard2.setLED(1)
ard2.disconnect()

#app = SampleApp()
#app.mainloop()