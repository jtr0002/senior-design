import tkinter as tk
import sys
import time
from random import randint
import tkinter.font as font
from tkinter import filedialog, Text
import threading
from Arduino import *


# Password Check this is a function called within password screen interface used to check password entered in box
def password_check():
    password = Enter_Password.get()
    if password == "Correct":
        raise_frame(Standard)


def no_fire_detection_system():
    global stop
    global alarm_status
    if alarm_status==0:
        alarm_status=1
    elif alarm_status==1:
        alarm_status=0
    stop = "false"


# Show_Contact is a function that adds an emergency contact to the list of emergency contacts in the settings
def show_contact():
    contact = Enter_Emergency_Contact.get()
    Number = Enter_Number.get()
    tk.Label(Emergency_Contact_Frame, text = "%s: %s" %(contact, Number)).grid(column=0)


def react_to_fire():
    if OSS == "connected":
        root.after(1, chirp_alarm())
        occupant_system.setLED(1)
    raise_frame(Fire_Detected)
    active['text'] = "Fire Detected"
    active['bg'] = "#e71414"
    
    
def chirp_alarm():
    global alarm_status
    if alarm_status==1:
        time.sleep(.05)
        occupant_system.soundAlarm()
        time.sleep(.05)
        occupant_system.stopAlarm()


# Timed_Events is the main loop to perform time based events within the program
# This Function only changes frame color and calls the react_to_fire alarm functions in case of a fire
def timed_events():
    alarm_status_text = "No Fire Detected"
    alarm_status_color = "#32CD32"
    global alarm_status
    global stop
    # Gets Fire Detection from FDS if available and randomly switches every 20 seconds if not
    if FDS == "connected":
        status=fire_detection_system.getCode(3)
        alarm_status = int.from_bytes(status,'big')
    if FDS =="disconnected" and stop!="true":
        stop="true"
        root.after(20000, no_fire_detection_system)

    # I don't know what outputs from Fire Detection System Look like now but I have zeros and ones for the second
    # By zero, I currently mean the value for no alarm, current it stops the alarm if no fire is detected
    if alarm_status == 0:
        alarm_status_text = "No Fire Detected"
        alarm_status_color = "#32CD32"
        if OSS=="connected":
            occupant_system.stopAlarm()
            occupant_system.setLED(0)
        active['text'] = "Alarm Resolved"
        active['bg'] = alarm_status_color
            
    # By alarm_status = 1, I mean the value for a detected fire, currently it sounds the alarm and sets the LED
    elif alarm_status == 1:
        alarm_status_text = "Emergency Fire Alarm"
        alarm_status_color = "#e71414"
        react_to_fire()
    Variable_Frame['text'] = alarm_status_text
    Variable_Frame['bg'] = alarm_status_color
    root.after(1000, timed_events)


# Raise Frame is the function that swaps which frame is visible on the tkinter window
def raise_frame(frame):
    frame.tkraise()


# The following two lines of code configure the window, its background color, and its size
root = tk.Tk()
root.configure(bg="white", height=500, width = 500)


# The following lines of code define all different frames of the interface
Opening_Screen = tk.Frame(root,bg="white")
Standard = tk.Frame(root, bg="white")
Settings = tk.Frame(root, bg="white")
Password_Frame = tk.Frame(root, bg="white")
Emergency_Contact_Frame = tk.Frame(root, bg="white")
Fire_Detected = tk.Frame(root, bg="white")


# This for statement creates an array of frames so that the raise_frame function can swap them
for frame in (Opening_Screen, Standard, Settings, Password_Frame, Password_Frame, Emergency_Contact_Frame, Fire_Detected):
    frame.grid(row=0, column=0, sticky='news')


# Opening_Screen Configuration (All Labels, Buttons, and Entrys)
opening_title = tk.Label(Opening_Screen, height=5, width=100, text="Fire Safety System", font=200, fg="black",
                         bg="white", bd=0).grid()
opening_spacing = tk.Label(Opening_Screen, height=15, width=45, font=200, fg="black", bg="white", bd=0).grid()
Password_Enter_Button = tk.Button(Opening_Screen, text="Open Your Fire Safety System", padx=10, pady=5, fg="black",
                                  bg="#b0e0e6", command=lambda: raise_frame(Password_Frame)).grid()


# Password Frame Configuration (All Labels, Buttons, and Entrys)
Password_Title = tk.Label(Password_Frame, height=5, width=100, text="Please Enter Password", font=200, fg="black",
                          bg="white", bd=0).grid()
Enter_Password = tk.Entry(Password_Frame,show='*')
Enter_Password.grid()
Submit = tk.Button(Password_Frame, text="Submit Password", fg="black", command=password_check).grid()


# Standard Frame Configuration (All Labels, Buttons, and Entrys)
tk.Label(Standard, height=7, width=100, text="Fire Alarm Monitor", font=200, fg="black", bg="white", bd=0).grid()
Variable_Frame = tk.Label(Standard, text="No Fire Detected", height=10, width=25, font=200, fg="black", bg="#32CD32",
                          bd=10, relief=tk.RAISED)
Variable_Frame.grid()
tk.Label(Standard, height=5, width=25, font=200, fg="black", bg="white", bd=0).grid()
tk.Button(Standard, text="Settings", padx=10, pady=5, fg="black", bg="#c2c5cc",
          command=lambda:raise_frame(Settings)).grid()
tk.Button(Standard, text="Back to Login", padx=10, pady=5, fg="black", bg="#c2c5cc",
          command=lambda: raise_frame(Opening_Screen)).grid()


# Settings Frame Configuration (All Labels, Buttons, and Entrys)
tk.Label(Settings, height=5, width=100, text="Settings Page", font=200, fg="black", bg="white", bd=0).grid()
tk.Label(Settings, height=10, width=45, font=200, fg="black", bg="white", bd=0).grid()
tk.Button(Settings, text="Change Password", padx=10, pady=5, fg="black", bg="#c2c5cc").grid()
tk.Button(Settings, text="Change Contact Settings", fg="black", bg="#c2c5cc",
          command=lambda: raise_frame(Emergency_Contact_Frame)).grid()
tk.Button(Settings, text="Back to Standard", padx=10, pady=5, fg="black", bg="#c2c5cc",
          command=lambda: raise_frame(Standard)).grid()


# Change User Contact Frame Configuration(All Labels, Buttons, Entries, and Actions
tk.Label(Emergency_Contact_Frame, height=0, width=50, text="Current Contacts",font=200, fg="black",
                        bg="white", bd=0).grid(row=1, column=0)
tk.Label(Emergency_Contact_Frame , height=0, width=50, text="Enter Emergency Contact",font=200, fg="black",
                        bg="white", bd=0).grid(row=1, column =1)
Enter_Emergency_Contact = tk.Entry(Emergency_Contact_Frame)
Enter_Emergency_Contact.grid(row=2, column =1)
tk.Label(Emergency_Contact_Frame , height=5, width=50, text="Enter Phone Number", font=200, fg="black",
                        bg="white", bd=0).grid(row=3, column =1)
Enter_Number = tk.Entry(Emergency_Contact_Frame)
Enter_Number.grid(row=4, column =1)
Submit = tk.Button(Emergency_Contact_Frame, text="Submit Contact", fg="black",
                   command=show_contact).grid(row=5, column=1)
tk.Button(Emergency_Contact_Frame, text="Back to Settings", padx=10, pady=5, fg="black", bg="#c2c5cc",
          command=lambda:raise_frame(Settings)).grid(column=1)


# Fire_Detected Configuration (All Labels, Buttons, and Entrys)
tk.Label(Fire_Detected, height=5, width=100, text="Fire Alarm", font=200, fg="black", bg="white", bd=0).grid()
active=tk.Label(Fire_Detected, text="Active Fire Alarm", height=10, width=25, font=200, fg="black", bg="#e71414", bd=10,
          relief= tk.RAISED)
active.grid()
tk.Label(Fire_Detected, height=5, width=25, font=200, fg="black", bg="white", bd=0).grid()
tk.Button(Fire_Detected, text="Return to Home", padx=10, pady=5, fg="black", bg="#c2c5cc",
          command=lambda: raise_frame(Opening_Screen)).grid()
tk.Button(Fire_Detected, text="Call For Help", padx=10, pady=5, fg="black", bg="#c2c5cc")


# Initial Raising of Opening Screen Frame
raise_frame(Opening_Screen)

FDS = "connected"
OSS = "connected"

# This stuff is added just to make the system run without FDS and OSS
global alarm_status
global stop
alarm_status = 0
stop = "false"


# In Use if entire system is functional
if (FDS == "connected"):
    fire_detection_system = Arduino("FSSModule", "e9:51:14:af:bb:16","Living")
    fire_detection_system.connect()
if (OSS == "connected"):
    occupant_system = Arduino('OSSModule', "ca:4a:27:6d:e5:29")
    occupant_system.connect()


# This function should be timed_events() when testing arduino
timed_events()


# Creates Loop for Window
root.mainloop()