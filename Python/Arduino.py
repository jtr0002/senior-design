import sys
sys.path.insert(1,'/usr/local/lib/python2.7/dist-packages/bluepy')
from btle import *

class Arduino:
    #blueAddr: Bluetooth Address
    #name: Name of Arduino
    def __init__(self, name, blueAddr, roomName="Null"):
        self.name = name
        self.blueAddr = blueAddr
        self.OSSMod1 = None
        self.characteristics = None
        self.roomName = roomName
        
    def connect(self):
        self.OSSMod1=Peripheral(self.blueAddr,ADDR_TYPE_PUBLIC)
        self.characteristics = self.OSSMod1.getCharacteristics()
       
    def getCode(self, index):
        if not(self.characteristics[index].supportsRead()) :
            return None
        
        char=self.characteristics[index]
        return char.read()
    
    def setLED(self, code):
        if not(self.characteristics[3].supportsRead()) :
            return None
        
        char=self.characteristics[3]
        char.write(hex(code)[2:].encode('UTF-8'), True)
        
    def setCode(self, index, code):
        if not(self.characteristics[index].supportsRead()) :
            return None
        
        char=self.characteristics[index]
        char.write(hex(code)[2:].encode('UTF-8'), True)
        
    def soundAlarm(self):
        if not(self.characteristics[4].supportsRead()) :
            return None
        
        char=self.characteristics[4]
        char.write(hex(1)[2:].encode('UTF-8'), True)
        
    def stopAlarm(self):
        if not(self.characteristics[4].supportsRead()) :
            return None
        
        char=self.characteristics[4]
        char.write(hex(0)[2:].encode('UTF-8'), True)
        
    def disconnect(self):
        self.OSSMod1.disconnect()
        