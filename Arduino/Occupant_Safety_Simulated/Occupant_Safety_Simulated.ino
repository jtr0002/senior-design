#include <ArduinoBLE.h>

BLEService OccupantSafety("7e17fe33-cff7-429d-97bf-5709802c17dd");

BLEByteCharacteristic ledIndicator("d67b33d8-0277-46ac-9469-90448cd38af0", BLERead | BLEWrite);
BLEByteCharacteristic buzzerIndicator("6fa478c5-9c2e-4c85-a0af-0c39e2472271", BLERead | BLEWrite);

byte Value;
String Text;

void setup() {
  BLE.begin();
  Serial.begin(9600);

  OccupantSafety.addCharacteristic(ledIndicator);
  OccupantSafety.addCharacteristic(buzzerIndicator);

  BLE.addService(OccupantSafety);

  ledIndicator.writeValue(0x00);
  buzzerIndicator.writeValue(0x00);

  BLE.advertise();
}

void loop() {
  BLEDevice central = BLE.central();

  if (central) {

    while (central.connected()) {
  if (ledIndicator.value() != 0x00) {
    String Text = "Led Value";
    byte Value = ledIndicator.value();
    Serial.println(Text);
    Serial.println(Value);
}
    if (buzzerIndicator.value() != 0x00) {
    String Text = "Buzzer Value";
    byte Value = buzzerIndicator.value();
    Serial.println(Text);
    Serial.println(Value);
}
delay(2000);
}
}
}
