/*
  Output the temperature readings to all pixels to be read by a Processing visualizer
  By: Nathan Seidle
  SparkFun Electronics
  Date: May 22nd, 2018
  License: MIT. See license file for more information but you can
  basically do whatever you want with this code.
  Feel like supporting open source hardware?
  Buy a board from SparkFun! https://www.sparkfun.com/products/14769
  This example outputs 768 temperature values as fast as possible. Use this example
  in conjunction with our Processing visualizer.
  This example will work with a Teensy 3.1 and above. The MLX90640 requires some
  hefty calculations and larger arrays. You will need a microcontroller with 20,000
  bytes or more of RAM.
  This relies on the driver written by Melexis and can be found at:
  https://github.com/melexis/mlx90640-library
  Hardware Connections:
  Connect the SparkFun Qwiic Breadboard Jumper (https://www.sparkfun.com/products/14425)
  to the Qwiic board
  Connect the male pins to the Teensy. The pinouts can be found here: https://www.pjrc.com/teensy/pinout.html
  Open the serial monitor at 115200 baud to see the output
*/


#include <Wire.h>
#include <ArduinoBLE.h>

#include "MLX90640_API.h"
#include "MLX90640_I2C_Driver.h"

#define TA_SHIFT 8 //Default shift for MLX90640 in open air


//Variable Initialization

const byte MLX90640_address = 0x33; //Default 7-bit unshifted address of the MLX90640
float mlx90640To[768];
float mlx90640To2[768];
paramsMLX90640 mlx90640;


//BLE Service and Char Initialization

BLEService FireDetection("a9fe7cb7-3970-4500-aa71-2b1115113c16");

BLEByteCharacteristic FireDetected("e3511235-bdcb-4713-9424-85a3322e457c" , BLEWrite | BLERead | BLENotify);


void setup() {

  Serial.begin(115200);


  //I2C Communication Begin

  Wire.begin();
  Wire.setClock(400000); //Increase I2C clock speed to 400kHz


  //Get device parameters - We only have to do this once

  uint16_t eeMLX90640[832];
  MLX90640_DumpEE(MLX90640_address, eeMLX90640);
  MLX90640_ExtractParameters(eeMLX90640, &mlx90640);

  MLX90640_SetRefreshRate(MLX90640_address, 0x03); //Set rate to 4Hz


  //Initialize Service and Char Names

  BLE.begin();

  BLE.setLocalName("FSSModule");
  BLE.setAdvertisedService(FireDetection);

  FireDetection.addCharacteristic(FireDetected);
  BLE.addService(FireDetection);

  FireDetected.writeValue(0x00);

  BLE.advertise();

}


void loop() {
  
  BLEDevice central = BLE.central();

  while (central.connected()) {


    // Collect Temp Array from Sensor and Store in mlx90640To

    for (byte x = 0 ; x < 2 ; x++) {

      uint16_t mlx90640Frame[834];
      int status = MLX90640_GetFrameData(MLX90640_address, mlx90640Frame);

      float vdd = MLX90640_GetVdd(mlx90640Frame, &mlx90640);
      float Ta = MLX90640_GetTa(mlx90640Frame, &mlx90640);

      float tr = Ta - TA_SHIFT; //Reflected temperature based on the sensor ambient temperature
      float emissivity = 0.95;

      MLX90640_CalculateTo(mlx90640Frame, &mlx90640, emissivity, tr, mlx90640To);
    }


    //Count Number of Pixels Above 80 Degrees C

    int count = 0;
    int sum = 0;
    int avg = 0;

    for (int x = 0; x < 768; x++) {
      if (mlx90640To[x] > 80) {
        count = count + 1;
        sum = sum + int(mlx90640To[x]);
      }
    }

    avg = sum / count;

    if (count >= 10) {

      delay(2000);


      //Re-check Data and Store as mlx90640To2

      for (byte x = 0 ; x < 2 ; x++) {
        uint16_t mlx90640Frame2[834];
        int status = MLX90640_GetFrameData(MLX90640_address, mlx90640Frame2);

        float vdd = MLX90640_GetVdd(mlx90640Frame2, &mlx90640);
        float Ta = MLX90640_GetTa(mlx90640Frame2, &mlx90640);

        float tr = Ta - TA_SHIFT; //Reflected temperature based on the sensor ambient temperature
        float emissivity = 0.95;

        MLX90640_CalculateTo(mlx90640Frame2, &mlx90640, emissivity, tr, mlx90640To2);
      }


      //Average Hot Pixels for Both Arrays and Find Difference

      int count2 = 0;
      int sum2 = 0;
      int avg2 = 0;

      for (int x = 0; x < 768; x++) {
        if (mlx90640To2[x] > 80) {
          count2 = count2 + 1;
          sum2 = sum2 + int(mlx90640To2[x]);
        }
      }

       if (count2!=0) {
        avg2 = sum2 / count2;
       }

      else {
        avg2=0;
      }


      //Check Difference in Averages and Notify If Greater Than 5

      int avgdiff = avg2 - avg;

      if (avgdiff >= -10) {
        FireDetected.writeValue((byte)0x01);
      }

      else {
        FireDetected.writeValue((byte)0x00);
      }
    }

    else {
      FireDetected.writeValue((byte)0x00);
    }

  }
}
