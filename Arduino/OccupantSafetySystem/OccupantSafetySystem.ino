#include <ArduinoBLE.h>

//Declare Digital Output Pins

int ledA = 12;
int ledB = 11;
int ledC = 10;
int ledD = 9;
int ledE = 8;
int ledF = 7;
int ledG = 6;
int ledH = 5;
int buzzer = 13;


//Declare LED Status Variables

int ledStatus;

BLEService OccupantSafety("7e17fe33-cff7-429d-97bf-5709802c17dd");

BLEByteCharacteristic ledIndicator("d67b33d8-0277-46ac-9469-90448cd38af0", BLERead | BLEWrite);
BLEByteCharacteristic buzzerIndicator("6fa478c5-9c2e-4c85-a0af-0c39e2472271", BLERead | BLEWrite);

void setup() {

  //Set Pins as Outputs

  pinMode(ledA, OUTPUT);
  pinMode(ledB, OUTPUT);
  pinMode(ledC, OUTPUT);
  pinMode(ledD, OUTPUT);
  pinMode(ledE, OUTPUT);
  pinMode(ledF, OUTPUT);
  pinMode(ledG, OUTPUT);
  pinMode(ledH, OUTPUT);
  pinMode(buzzer, OUTPUT);


  //BLE Initialization

  BLE.begin();


  //Set BLE Names and Start Advertising

  BLE.setLocalName("OSSModule");
  BLE.setAdvertisedService(OccupantSafety);

  OccupantSafety.addCharacteristic(ledIndicator);
  OccupantSafety.addCharacteristic(buzzerIndicator);

  BLE.addService(OccupantSafety);

  ledIndicator.writeValue(0x00);
  buzzerIndicator.writeValue(0x00);

  BLE.advertise();

}


void loop() {

BLEDevice central = BLE.central();

if (central) {

  while (central.connected()) {

  //Display Left Arrow

  if (ledIndicator.value() == 0x31) {
    digitalWrite(ledA, 1);
    digitalWrite(ledB, 1);
    digitalWrite(ledC, 1);
    digitalWrite(ledD, 1);
    digitalWrite(ledE, 1);
    digitalWrite(ledF, 0);
    digitalWrite(ledG, 1);
    digitalWrite(ledH, 0);
  }


  //Display Right Arrow

  else if (ledIndicator.value() == 0x32) {
    digitalWrite(ledA, 0);
    digitalWrite(ledB, 1);
    digitalWrite(ledC, 0);
    digitalWrite(ledD, 1);
    digitalWrite(ledE, 1);
    digitalWrite(ledF, 1);
    digitalWrite(ledG, 1);
    digitalWrite(ledH, 1);
  }


  //Display Bi-directional Arrow

  else if (ledIndicator.value() == 0x33) {
    digitalWrite(ledA, 1);
    digitalWrite(ledB, 1);
    digitalWrite(ledC, 1);
    digitalWrite(ledD, 1);
    digitalWrite(ledE, 1);
    digitalWrite(ledF, 1);
    digitalWrite(ledG, 1);
    digitalWrite(ledH, 1);
  }


  //Display No Arrow

  else {
    digitalWrite(ledA, 0);
    digitalWrite(ledB, 0);
    digitalWrite(ledC, 0);
    digitalWrite(ledD, 0);
    digitalWrite(ledE, 0);
    digitalWrite(ledF, 0);
    digitalWrite(ledG, 0);
    digitalWrite(ledH, 0);
  }


  //Pinging of Piezo Buzzer

  if (buzzerIndicator.value() == 0x31) {
    digitalWrite(buzzer, 1);
    delay(1000);
    digitalWrite(buzzer, 0);
  }

  else {
    digitalWrite(buzzer, 0);
  }
}
}
}
