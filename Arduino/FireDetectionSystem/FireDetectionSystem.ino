/*
  Output the temperature readings to all pixels to be read by a Processing visualizer
  By: Nathan Seidle
  SparkFun Electronics
  Date: May 22nd, 2018
  License: MIT. See license file for more information but you can
  basically do whatever you want with this code.
  Feel like supporting open source hardware?
  Buy a board from SparkFun! https://www.sparkfun.com/products/14769
  This example outputs 768 temperature values as fast as possible. Use this example
  in conjunction with our Processing visualizer.
  This example will work with a Teensy 3.1 and above. The MLX90640 requires some
  hefty calculations and larger arrays. You will need a microcontroller with 20,000
  bytes or more of RAM.
  This relies on the driver written by Melexis and can be found at:
  https://github.com/melexis/mlx90640-library
  Hardware Connections:
  Connect the SparkFun Qwiic Breadboard Jumper (https://www.sparkfun.com/products/14425)
  to the Qwiic board
  Connect the male pins to the Teensy. The pinouts can be found here: https://www.pjrc.com/teensy/pinout.html
  Open the serial monitor at 115200 baud to see the output
*/

#include <Wire.h>
#include <ArduinoBLE.h>

#include "MLX90640_API.h"
#include "MLX90640_I2C_Driver.h"

#define TA_SHIFT 8 //Default shift for MLX90640 in open air


  //Variable Initialization

  const byte MLX90640_address = 0x33; //Default 7-bit unshifted address of the MLX90640
  byte fireStatus;
  float mlx90640To[768];
  int mlxFrame[32][24];
  int mlxFrame2[32][24];
  int mlxDiff[32][24];
  int mlxPool[8][6];
  int a=0;
  int b=0;
  
  paramsMLX90640 mlx90640;


  //BLE Service and Char Initialization

  BLEService FireDetection("a9fe7cb7-3970-4500-aa71-2b1115113c16");

  BLEByteCharacteristic FireDetected("e3511235-bdcb-4713-9424-85a3322e457c" , BLEWrite | BLERead | BLENotify);


void setup() {

  //BLE Begin

  BLE.begin();

  
  //I2C Communication Begin
  
  Wire.begin();
  Wire.setClock(400000); //Increase I2C clock speed to 400kHz
  

  //Get device parameters - We only have to do this once
  
  int status;
  uint16_t eeMLX90640[832];
  status = MLX90640_DumpEE(MLX90640_address, eeMLX90640);

  MLX90640_SetRefreshRate(MLX90640_address, 0x03); //Set rate to 4Hz
  

  //Initialize Service and Char Names
  
  BLE.begin();

  BLE.setLocalName("FSSModule");
  BLE.setAdvertisedService(FireDetection);

  FireDetection.addCharacteristic(FireDetected);
  BLE.addService(FireDetection);

  BLE.advertise();
  
}

void loop() {

  start:

  // Collect Temp Array from Sensor and Store in mlx90640To
  
  for (byte x = 0 ; x < 2 ; x++) {
    
    uint16_t mlx90640Frame[834];
    int status = MLX90640_GetFrameData(MLX90640_address, mlx90640Frame);

    float vdd = MLX90640_GetVdd(mlx90640Frame, &mlx90640);
    float Ta = MLX90640_GetTa(mlx90640Frame, &mlx90640);

    float tr = Ta - TA_SHIFT; //Reflected temperature based on the sensor ambient temperature
    float emissivity = 0.95;

    MLX90640_CalculateTo(mlx90640Frame, &mlx90640, emissivity, tr, mlx90640To);
  }


  //Initial Check to Check Pixels for Temp>200C

  for (int x=0; x<768; x++){
    if (mlx90640To[x]>200) {
      goto frame;
    }
  } 
  
  fireStatus=0x00;
  FireDetected.writeValue(fireStatus);
  goto start;

  
  //Compile Data Into Real Frame

  frame:
  a=0;
  
  for (int x=0; x<32; x++){
    for (int y=0; y<24; y++){
     mlxFrame[x][y]=int(mlx90640To[a]);
     a++;
    }
  } 


  //Max Pooling (4x4) on Frame Data

  a=0;
  b=0;

  for (int x=0; x<8; x++) {
    for (int y=0; y<6; y++) {
      a=x*4;
      b=y*4;
      mlxPool[x][y]=(mlxFrame[a][b]+mlxFrame[a][b+1]+mlxFrame[a][b+2]+mlxFrame[a][b+3]+mlxFrame[a+1][b]+mlxFrame[a+1][b+1]+mlxFrame[a+1][b+2]
      +mlxFrame[a+1][b+3]+mlxFrame[a+2][b]+mlxFrame[a+2][b+1]+mlxFrame[a+2][b+2]+mlxFrame[a+2][b+3]+mlxFrame[a+3][b]+mlxFrame[a+3][b+1]+mlxFrame[a+3][b+2]
      +mlxFrame[a+3][b+3])/16;
    }
  }


  //Check New Matrix for Temp>200C
  
  for (int x=0; x<8; x++){
    for (int y=0; y<6; y++){
      if (mlxPool[x][y]>200) {
        goto diff;
      }
    }
  }
  
       fireStatus=0x00;
       FireDetected.writeValue(fireStatus);
       goto start;


  //Check Difference Between Last Two Frames

  diff:
  
  for (byte x = 0 ; x < 2 ; x++) {
    
    uint16_t mlx90640Frame[834];
    int status = MLX90640_GetFrameData(MLX90640_address, mlx90640Frame);

    float vdd = MLX90640_GetVdd(mlx90640Frame, &mlx90640);
    float Ta = MLX90640_GetTa(mlx90640Frame, &mlx90640);

    float tr = Ta - TA_SHIFT; //Reflected temperature based on the sensor ambient temperature
    float emissivity = 0.95;

    MLX90640_CalculateTo(mlx90640Frame, &mlx90640, emissivity, tr, mlx90640To);
    
  }

  a=0;
  
  for (int x=0; x<32; x++){
    for (int y=0; y<24; y++){
      
     mlxFrame2[x][y]=mlx90640To[a];
     mlxDiff[x][y]=mlxFrame2[x][y]-mlxFrame[x][y];

      if (mlxDiff[x][y]>3) {
        goto alert;
      }
      
     a++;
     
    }
  } 

  fireStatus=0x00;
  FireDetected.writeValue(fireStatus);
  goto start;

    
  //Write Characteristic Value to 1 if Passes All Criteria for Fire
  
  alert:
  
  fireStatus=0x01;
  FireDetected.writeValue(fireStatus);
}
