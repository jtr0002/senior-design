#include <Wire.h>
#include <ArduinoBLE.h>

byte fireStatus;
byte alternator;

  BLEService FireDetection("a9fe7cb7-3970-4500-aa71-2b1115113c16");
  BLEByteCharacteristic FireDetected("e3511235-bdcb-4713-9424-85a3322e457c" , BLEWrite | BLERead | BLENotify);
  
void setup() {
  // BLE begin

  BLE.begin();
  Serial.begin(9600);
  
  BLE.setLocalName("FSSModule");
  BLE.setAdvertisedService(FireDetection);
  
  FireDetection.addCharacteristic(FireDetected);
  BLE.addService(FireDetection);
  BLE.advertise();
  BLEDevice central = BLE.central();
  alternator=0x00;
}

void loop() {
  
      BLEDevice central = BLE.central();
        while (central.connected()) {
          
          if (alternator == 0x00) {
          fireStatus = 0x01;
          Serial.println(fireStatus);
          }

          else {
          fireStatus = 0x00;
          Serial.println(fireStatus);
          }
          
          
      alternator = fireStatus;
      FireDetected.writeValue(fireStatus);
      delay(2000);
}
}
    
